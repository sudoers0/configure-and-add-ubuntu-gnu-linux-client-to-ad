#!/bin/bash

# TERMINAL COLORS
WARNINGCOLOR="\033[1;91m"
SUCCESSCOLOR="\033[1;92m"
NORMALCOLOR="\033[0m"


#### CONFIGURE THIS VARIABLES TO YOUR NECESITY ####
# Domain
domain="domain.com"


# Authentication user
userAD="administrator"

# GNU/Linux -GROUP- of users who can log in (YOU MUST CREATE A GROUP WITH USERS THAT YOU WANT TO ALLOW TO LOG IN)
group="group"

# Local user, your normal user
localUser=$(echo $LOGNAME)
#localUser=ubuntuuser # Uncomment this line and enter manually the username if you have problems

# Network config
#If you have problems try to comment the line below ($conName variable) and
# uncomment the following (replace the name of the network connection with the name of your connection)
conName=$(nmcli -f name connection show --active | head -n 2 | tail -n 1 | xargs)
#conName="Conexión cableada 1"

DNS1="192.168.1.100"
DNS2="208.67.222.222"

# Change this to yes when the VARIABLES ADOVE are properly configured
continueConf=no



#### THE SCRIPT START HERE ####

# If the user isn't superuser
if [[ "$(id -u)" != "0" ]]; then
    echo -e "${WARNINGCOLOR}MUST BE EXECUTED AS ROOT :)"
    exit 1

else
    # If user have changed the variables
    if [[ $continueConf == "yes" ]]; then
        echo CONTINUING...
    else
    	echo -e "\n${WARNINGCOLOR}To run this script you must have configure it," \
        	"open it with any text editor enter your values to the variables${NORMALCOLOR}\n"
        exit 1
    fi


    # Add repo, key and install pbis-open
    wget -O - http://repo.pbis.beyondtrust.com/apt/RPM-GPG-KEY-pbis|sudo apt-key add -
    wget -O /etc/apt/sources.list.d/pbiso.list http://repo.pbis.beyondtrust.com/apt/pbiso.list
    # Update and upgrade the system
    apt update && apt upgrade -y
    apt install ssh pbis-open -y

    # Change DNS
    nmcli connection modify "$conName" ipv4.ignore-auto-dns yes
    nmcli connection modify "$conName" ipv4.dns "$DNS1 $DNS2"

    nmcli connection down "$conName"
    nmcli connection up "$conName"

    # Add pc to the domain
    domainjoin-cli join $domain $userAD

    # For what?
    service ssh restart


    # Config log in options for AD
    /opt/pbis/bin/config UserDomainPrefix $domain
    /opt/pbis/bin/config AssumeDefaultDomain true
    /opt/pbis/bin/config LoginShellTemplate /bin/bash
    /opt/pbis/bin/config HomeDirTemplate %H/%U
    # Create new group on AD for GNU/Linux users
    /opt/pbis/bin/config RequireMembershipOf $domain\\$group


    # Block guest log in and enable manual log in
    echo 'allow-guest=false
    greater-show-manual-login=true' >> /usr/share/lightdm/lightdm.conf.d/50-ubuntu.conf # VARÍA SEGUN LA DISTRO

    # Add to administrator to sudoers
    usermod -aG sudo $(echo $domain | cut -f 1 -d '.' | awk '{print toupper($0)}')\\$userAD

    # Hide user on log in screen
    sed -ie 's/\(SystemAccount=\).*/\1true/' /var/lib/AccountsService/users/$localUser # VARÍA SEGUN LA DISTRO

    # Reboot system
    echo -e "\n${SUCCESSCOLOR}Reboot your system for changes to effect${NORMALCOLOR}\n"
fi

