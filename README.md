# Configure and add Ubuntu GNU-Linux client to Active Directory

![Ubuntu Client AD](/images/ubuntu1810.png)

## Configure and install

Download script

```bash
wget https://gitlab.com/sudoers0/configure-and-add-ubuntu-gnu-linux-client-to-ad/raw/master/pbis_sudoers1.com.sh
```

Configure the variable of script with your configuration

```bash
nano pbis_sudoers1.com.sh
```

Give execution permisions and execute it as superuser

```bash
chmod +x pbis_sudoers1.com.sh 
sudo ./pbis_sudoers1.com.sh
```


![Linux mint Client AD](/images/mint19.png)

## Troubleshooting

If you get an error like:

```bash
sed: can't read /var/lib/AccountsService/users/root: No such file or directory
```

You must open the script and read the description of '$localUser' and make the changes

![Mint username sed error](/images/error-mint-sed.png)

